import 'package:clean_application/clean_application.dart';

class User extends Entity<String> {
  String name;

  User({this.name, String id}) : super(id: id);
}
