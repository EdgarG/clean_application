import 'package:clean_application/clean_application.dart';
import 'package:flutter/cupertino.dart';

import '../entities/user.dart';
import '../repository_contracts/user_repository.dart';

class GetUserParams extends UseCaseParam {
  String id;

  GetUserParams({@required this.id});
}

class GetUserUseCase extends FutureUseCase<User, GetUserParams> {
  UserRepository userRepository;

  GetUserUseCase(this.userRepository);

  Future<Result<User>> call(GetUserParams params) {
    return userRepository.findById(params.id);
  }
}
