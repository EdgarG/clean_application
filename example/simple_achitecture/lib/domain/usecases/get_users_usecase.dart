import 'package:clean_application/clean_application.dart';

import '../entities/user.dart';
import '../repository_contracts/user_repository.dart';

class GetUsersParam extends UseCaseParam {
  GetUsersParam();
}

class GetUsersUseCase extends FutureUseCase<List<User>, GetUsersParam> {
  UserRepository userRepository;

  GetUsersUseCase(this.userRepository);

  @override
  Future<Result<List<User>>> call(GetUsersParam params) async {
    var res = await userRepository.findAll();
    return Result.success(res.data);
  }
}
