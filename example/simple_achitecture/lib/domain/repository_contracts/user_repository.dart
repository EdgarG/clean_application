import 'package:clean_application/clean_application.dart';

import '../entities/user.dart';

abstract class UserRepository extends CrudRepository<User, String> {}
