import 'dart:developer';

import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/service.dart';
import 'package:simple_achitecture/presentation/home_view.dart';
import 'package:simple_achitecture/presentation/second/second_controller.dart';
import 'package:simple_achitecture/presentation/second/second_view.dart';

import '../data/datasources/user_datasource.dart';
import '../data/datasources/user_datasource_prod.dart';
import '../data/datasources/user_fake_datasource.dart';
import '../data/repositories/user_repository_impl.dart';
import '../domain/repository_contracts/user_repository.dart';
import '../domain/usecases/get_user_usecase.dart';
import '../domain/usecases/get_users_usecase.dart';
import '../presentation/home_controller.dart';

class AppConfiguration extends Configuration {
  @override
  List<UseCase> usecases() {
    return [
      GetUserUseCase(CleanApplication.repository<UserRepository>()),
      GetUsersUseCase(CleanApplication.repository<UserRepository>()),
    ];
  }

  @override
  List<Factory<Controller>> controllers() {
    return [
      Factory<HomeController>(factory: () => HomeController()),
      Factory<CounterController>(factory: () => CounterController()),
      Factory<ColorController>(factory: () => ColorController()),
      Factory<SecondController>(factory: () => SecondController()),
    ];
  }

  @override
  List<Implementation<DataSource>> datasources() {
    return [
      Implementation<UserDataSource>(instances: {
        Environments.Development: () => UserFakeDataSource(),
        Environments.Production: () => UserDataSourceProduction(),
      }),
    ];
  }

  @override
  List<Implementation<Repository>> repositories() {
    return [
      Implementation<UserRepository>(instances: {
        Environments.All: () =>
            UserRepositoryImpl(CleanApplication.datasource<UserDataSource>()),
      })
    ];
  }

  @override
  List<RouterBuilder> routes() {
    return [
      RouterBuilder<HomeView>(builder: (args) => HomeView()),
      RouterBuilder<SecondView>(
          builder: (args) => SecondView(title: args as String)),
    ];
  }

  @override
  List<Factory<Service>> services() {
    return null;
  }
}
