import 'dart:math';
import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:simple_achitecture/presentation/second/second_view.dart';

import 'home_controller.dart';

class HomeView extends View<HomeController> {
  @override
  Widget build(BuildContext context, HomeController controller) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TEST"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          controller.isInitialized
              ? Text(controller.user?.name ?? 'NO USER')
              : CircularProgressIndicator(),
          getUsers(),
          ColorContainer(),
          CounterWidget(),
          ResetCounterWidget(),
          FlatButton(
              onPressed: () =>
                  router.pushType(SecondView, arguments: 'New title :D'),
              child: Text('Second View'))
        ],
      ),
      floatingActionButton: CounterButton(),
    );
  }

  Widget getUsers() {
    if (!controller.hasUsers) {
      return Container();
    }

    return Column(
      children: controller.users.map((u) => Text(u.name)).toList(),
    );
  }
}

class CounterWidget extends DynamicComponent<CounterController> {
  @override
  Widget build(BuildContext context, CounterController controller) {
    return Container(
      color: Colors.green,
      width: 100,
      height: 100,
      child: Center(
        child: Text("${controller.count}"),
      ),
    );
  }
}

class CounterButton extends StaticComponent<CounterController> {
  @override
  Widget build(BuildContext context, CounterController controller) {
    return FloatingActionButton(
      onPressed: () => controller.setState(() => controller.increment()),
      tooltip: 'Increment',
      child: Icon(
        Icons.add,
        color: randomColor(),
      ),
    );
  }

  Color randomColor() {
    Random random = Random();
    return Color.fromRGBO(
        random.nextInt(250), random.nextInt(250), random.nextInt(250), 1);
  }
}

class ResetCounterWidget extends StaticComponent<CounterController> {
  @override
  Widget build(BuildContext context, CounterController controller) {
    return FlatButton(
        onPressed: () =>
            controller.setState(() => controller.onInitialize(null)),
        child: Text(
          'Reset Counter',
          style: TextStyle(color: randomColor()),
        ));
  }

  Color randomColor() {
    Random random = Random();
    return Color.fromRGBO(
        random.nextInt(250), random.nextInt(250), random.nextInt(250), 1);
  }
}

class ColorContainer extends DynamicComponent<ColorController> {
  @override
  Widget build(BuildContext context, ColorController controller) {
    return InkWell(
      onTap: () => controller.setState(() => controller.update()),
      child: Container(
        color: controller.color,
        height: 100,
        width: 100,
      ),
    );
  }
}
