import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:simple_achitecture/presentation/second/second_controller.dart';

// ignore: must_be_immutable
class SecondView extends AnimatedView<SecondController> {
  final String title;
  Animation _animation;

  SecondView({this.title}) : super(duration: Duration(seconds: 4));

  @override
  void onInitialize(AnimationController animationController) {
    _animation = Tween(begin: 0.0, end: 1.0).animate(animationController);
    animationController.forward();
  }

  @override
  Widget build(BuildContext context, SecondController controller,
      AnimationController animationController) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          Center(
            child: AnimatedBuilder(
                animation: _animation,
                builder: (context, _) {
                  return Opacity(opacity: _animation.value, child: Text(title));
                }),
          ),
          RaisedButton(
            onPressed: () {
              if (_animation.isCompleted) {
                animationController.reverse();
              } else {
                animationController.forward();
              }
            },
            child: Text('Animate'),
          ),
        ],
      ),
    );
  }
}
