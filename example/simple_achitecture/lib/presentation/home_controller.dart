import 'dart:math';
import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';

import '../domain/entities/user.dart';
import '../domain/usecases/get_user_usecase.dart';
import '../domain/usecases/get_users_usecase.dart';

class HomeController extends Controller {
  User user;
  List<User> _users;

  GetUserUseCase _getUserUseCase;
  GetUsersUseCase _getUsersUseCase;

  List<User> get users => _users;

  bool get hasUsers => users != null && users.isNotEmpty;

  @override
  Future onInitialize(dynamic args) async {
    _getUserUseCase = CleanApplication.usecase<GetUserUseCase>();
    _getUsersUseCase = CleanApplication.usecase<GetUsersUseCase>();

    await Future.delayed(Duration(seconds: 5));
    Result<User> result = await _getUserUseCase(GetUserParams(id: '1'));
    if (result.isSuccess) {
      user = result.data;
    } else {
      throw Exception(result.message);
    }

    Result<List<User>> res = await _getUsersUseCase(GetUsersParam());

    if (res.isSuccess) {
      _users = res.data;
    } else {
      throw Exception(res.message);
    }
  }
}

class CounterController extends Controller {
  int count;

  void increment() {
    count++;
  }

  @override
  Future onInitialize(dynamic args) async {
    count = 0;
  }
}

class ColorController extends Controller {
  Color color;
  Random random = Random();

  @override
  Future onInitialize(dynamic args) async {
    update();
  }

  void update() {
    color = Color.fromRGBO(
        random.nextInt(250), random.nextInt(250), random.nextInt(250), 1);
  }
}
