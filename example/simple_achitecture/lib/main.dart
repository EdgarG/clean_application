import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';

import 'core/setup.dart';
import 'presentation/home_view.dart';

void main() {
  runApp(CleanApplication(
    environment: Environments.Production,
    configuration: AppConfiguration(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Clean Application',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '$HomeView',
      navigatorKey: CleanApplication.navigatorKey,
      onGenerateRoute: CleanApplication.generateRoutes,
    );
  }
}
