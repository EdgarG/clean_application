import 'package:clean_application/clean_application.dart';

import '../../domain/entities/user.dart';
import '../../domain/repository_contracts/user_repository.dart';
import '../datasources/user_datasource.dart';

class UserRepositoryImpl extends UserRepository {
  final UserDataSource userDataSource;

  UserRepositoryImpl(this.userDataSource);

  @override
  Future<Result<int>> count() {
    return null;
  }

  @override
  Future<Result<bool>> delete(User entity) {
    // TODO: implement delete
    return null;
  }

  @override
  Future<Result<bool>> deleteAll() {
    // TODO: implement deleteAll
    return null;
  }

  @override
  Future<Result<bool>> deleteById(String id) {
    // TODO: implement deleteById
    return null;
  }

  @override
  Future<Result<bool>> deleteEntities(Iterable<User> entities) {
    // TODO: implement deleteEntities
    return null;
  }

  @override
  Future<Result<bool>> existsById(String id) {
    // TODO: implement existsById
    return null;
  }

  @override
  Future<Result<Iterable<User>>> findAll() async {
    var result = await userDataSource.findAll();

    if (result.isSuccess) {
      var list = result.data.map((model) => model.toEntity()).toList();
      return Result.success(list);
    }

    return Result.failure(result.message);
  }

  @override
  Future<Result<Iterable<User>>> findAllById(Iterable<String> ids) {
    // TODO: implement findAllById
    return null;
  }

  @override
  Future<Result<User>> findById(String id) async {
    try {
      var result = await userDataSource.findById(id);
      if (result.isSuccess) {
        return Result.success(result.data.toEntity());
      } else {
        throw Exception(result.message);
      }
    } catch (e) {
      return Result.failure(e?.message ?? "$runtimeType: Unknown error");
    }
  }

  @override
  Future<Result<User>> save(User entity) {
    // TODO: implement save
    return null;
  }

  @override
  Future<Result<Iterable<User>>> saveAll(Iterable<User> entities) {
    // TODO: implement saveAll
    return null;
  }
}
