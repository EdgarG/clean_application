import 'package:clean_application/clean_application.dart';

import '../../domain/entities/user.dart';

class UserModel implements ModelMap<User> {
  String id;
  String name;

  UserModel({this.id, this.name});

  @override
  Model<User, Map> fromDataSource(Map object) {
    return UserModel(
      id: object["id"],
      name: object["name"],
    );
  }

  @override
  Model<User, Map> fromEntity(User entity) {
    return UserModel(id: entity.id, name: entity.name);
  }

  @override
  User toEntity() {
    return User(
      id: id,
      name: name,
    );
  }

  @override
  Map toDataSource() {
    return {
      "id": id,
      "name": name,
    };
  }
}
