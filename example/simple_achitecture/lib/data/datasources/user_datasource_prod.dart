import 'package:clean_application/clean_application.dart';
import '../models/user_model.dart';
import 'user_datasource.dart';

class UserDataSourceProduction extends UserDataSource {
  List<UserModel> _entities = [
    UserModel(id: "1", name: "User 1"),
    UserModel(id: "2", name: "User 2"),
    UserModel(id: "3", name: "User 3"),
    UserModel(id: "3", name: "User 4"),
    UserModel(id: "3", name: "User 5"),
    UserModel(id: "3", name: "User 6"),
  ];

  @override
  Future<Result<int>> count() async {
    return Result.success(_entities.length);
  }

  @override
  Future<Result<bool>> delete(UserModel entity) async {
    return Result.success(_entities.remove(entity));
  }

  @override
  Future<Result<bool>> deleteAll() async {
    _entities.clear();
    return Result.success(true);
  }

  @override
  Future<Result<bool>> deleteById(String id) {
    // TODO: implement deleteById
    return null;
  }

  @override
  Future<Result<bool>> deleteEntities(Iterable<UserModel> entities) {
    // TODO: implement deleteEntities
    return null;
  }

  @override
  Future<Result<bool>> existsById(String id) {
    // TODO: implement existsById
    return null;
  }

  @override
  Future<Result<Iterable<UserModel>>> findAll() async {
    return Result.success(_entities);
  }

  @override
  Future<Result<Iterable<UserModel>>> findAllById(Iterable<String> ids) {
    // TODO: implement findAllById
    return null;
  }

  @override
  Future<Result<UserModel>> findById(String id) async {
    var model = _entities.firstWhere((m) => m.id == id);
    if (model != null) {
      return Result.success(model);
    }

    return Result.failure("UserModel with id: $id does not exist!");
  }

  @override
  Future<Result<UserModel>> save(UserModel entity) {
    // TODO: implement save
    return null;
  }

  @override
  Future<Result<Iterable<UserModel>>> saveAll(Iterable<UserModel> entities) {
    // TODO: implement saveAll
    return null;
  }
}
