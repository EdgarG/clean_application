import 'package:clean_application/clean_application.dart';

import '../models/user_model.dart';

abstract class UserDataSource extends CrudDataSource<UserModel, String> {}
