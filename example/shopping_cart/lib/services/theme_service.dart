import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/main.dart';

class ThemeService extends Service {
  ThemeData _data = ThemeData.light();

  ThemeData get currentTheme => _data;

  void change() {
    if (_data.brightness == Brightness.dark) {
      _data = ThemeData.light();
    } else {
      _data = ThemeData.dark();
    }

    MyAppController controller = CleanApplication.controller<MyAppController>();
    controller.rebuild();
  }

  static void changeTheme() {
    ThemeService themeService = CleanApplication.service<ThemeService>();
    themeService.change();
  }
}
