import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/presentation/components/item_card.dart';
import 'package:shopping_cart/presentation/controllers/home_controller.dart';
import 'package:shopping_cart/presentation/controllers/items_controller.dart';

class CartView extends View<ItemsController> {
  static const String Tag = 'cart';

  CartView() : super(tag: Tag);

  @override
  Widget build(BuildContext context, ItemsController controller) {
    Widget body;

    if (!controller.isInitialized) {
      body = Center(child: CircularProgressIndicator());
    } else {
      body = ListView(
        children: controller.itemsOnCart
            .map((item) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ItemCard(
                    item: item,
                  ),
                ))
            .toList(),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: controller.isInitialized
                ? Center(
                    child: Text(
                    '${controller.itemsOnCart.length}',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ))
                : Container(),
          ),
        ],
      ),
      body: body,
    );
  }

  Widget itemTile(Item item) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text('${item.name}'),
            Text('Count: ${item.count}'),
          ],
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FlatButton(
              onPressed: () =>
                  controller.setState(() => controller.updateItem(item, 1)),
              child: Text('Increment'),
            ),
            FlatButton(
              onPressed: () =>
                  controller.setState(() => controller.updateItem(item, -1)),
              child: Text('Decrement'),
            ),
            FlatButton(
              onPressed: () =>
                  controller.setState(() => controller.clearItem(item)),
              child: Text('Clear'),
            ),
          ],
        ),
      ],
    );
  }
}
