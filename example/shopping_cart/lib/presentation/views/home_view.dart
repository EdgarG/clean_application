import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/presentation/controllers/home_controller.dart';
import 'package:shopping_cart/presentation/views/items_stream_view.dart';
import 'package:shopping_cart/presentation/views/items_view.dart';

class HomeTagView extends View<HomeController> {
  @override
  Widget build(BuildContext context, HomeController controller) {
    if (!controller.isInitialized) {
      return Center(child: CircularProgressIndicator());
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Home View with Tag'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () => router.pushType(ItemsView),
              child: Text('Items View (Future)'),
            ),
            RaisedButton(
              onPressed: () => router.pushType(ItemsStreamView),
              child: Text('Items View (Stream)'),
            ),
            RaisedButton(
              onPressed: () => controller.changeTheme(),
              child: Text('Change Theme'),
            ),
          ],
        ),
      ),
    );
  }
}
