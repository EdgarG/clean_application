import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:shopping_cart/presentation/components/cart_icon.dart';
import 'package:shopping_cart/presentation/components/item_card.dart';
import 'package:shopping_cart/presentation/controllers/items_controller.dart';
import 'package:shopping_cart/services/theme_service.dart';

class ItemsView extends View<ItemsController> {
  @override
  Widget build(BuildContext context, ItemsController controller) {
    if (!controller.isInitialized) {
      return Center(child: CircularProgressIndicator());
    }

    if (!controller.hasItems) {
      return Center(child: Text('No items'));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Shopping Cart with Tag'),
        actions: <Widget>[
          CartIcon(),
          IconButton(
              icon: Icon(Icons.compare_arrows),
              onPressed: () => ThemeService.changeTheme()),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 5,
          children: controller.items
              .map(
                (item) => ItemCard(
                  item: item,
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
