import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/presentation/controllers/items_stream_controller.dart';
import 'package:shopping_cart/presentation/views/home_view.dart';
import 'package:shopping_cart/util/random_color.dart';

class ItemsStreamView extends StaticView<ItemsStreamController> {
  @override
  Widget build(BuildContext context, ItemsStreamController controller) {
    if (!controller.isInitialized) {
      return Center(child: CircularProgressIndicator());
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Shopping Cart Stream'),
        actions: <Widget>[
          _CartIcon(),
        ],
      ),
      body: StreamBuilder<List<Item>>(
          stream: controller.stream(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            }

            if (snapshot.hasError) {
              return Center(
                child: Text('${snapshot.error}'),
              );
            }

            if (snapshot.hasData) {
              var items = snapshot.data;

              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: GridView.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 5,
                  children: items.map((item) => getItemCard(item)).toList(),
                ),
              );
            }

            return Center(
              child: Text('Other state'),
            );
          }),
    );
  }

  Widget getItemCard(Item item) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 100,
          color: RandomColor.color(),
          child: Center(
            child: Text('${item.name}'),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            getItemButtom(item, false),
            Text(
              '${item.count}',
              style: TextStyle(color: RandomColor.color()),
            ),
            getItemButtom(item, true),
          ],
        ),
      ],
    );
  }

  Widget getItemButtom(Item item, bool isIncrementing) {
    if (isIncrementing) {
      return getButtom(
        true,
        Icons.arrow_right,
        () => controller.updateItem(item, 1),
      );
    }
    return getButtom(
      item.count > 0,
      Icons.arrow_left,
      () => controller.updateItem(item, -1),
    );
  }

  Widget getButtom(bool canUpdate, IconData iconData, VoidCallback callback) {
    return IconButton(
      icon: Icon(iconData),
      onPressed: canUpdate ? () => callback() : null,
    );
  }
}

class _CartIcon extends DynamicComponent<ItemsStreamController> {
  @override
  Widget build(BuildContext context, ItemsStreamController controller) {
    return InkWell(
      onTap: () => null,
      child: Container(
        alignment: Alignment.center,
        child: Stack(
          children: <Widget>[
            Icon(Icons.shopping_cart),
            getCountIndicator(),
          ],
        ),
      ),
    );
  }

  Widget getCountIndicator() {
    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        color: Colors.red,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(
        child: Text(
          '${controller.countItemsOnCart}',
          style: TextStyle(fontSize: 10),
        ),
      ),
    );
  }
}
