import 'dart:async';

import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_stream_repository.dart';
import 'package:shopping_cart/data/usecases/get_items_stream_usecase.dart';
import 'package:shopping_cart/data/usecases/update_item_count_stream_usecase.dart';

class ItemsStreamController extends Controller {
  GetItemsStreamUseCase getItemsStreamUseCase;
  UpdateItemCountStreamUseCase updateItemCountStreamUseCase;
  ItemsStreamRepository itemsStreamRepository;

  StreamSubscription subscription;

  int _nItemsOnCart = 0;
  get countItemsOnCart => _nItemsOnCart;

  @override
  Future onInitialize(args) async {
    getItemsStreamUseCase = CleanApplication.usecase<GetItemsStreamUseCase>();
    updateItemCountStreamUseCase =
        CleanApplication.usecase<UpdateItemCountStreamUseCase>();

    itemsStreamRepository =
        CleanApplication.repository<ItemsStreamRepository>();
    itemsStreamRepository.init();
    _updateItemsOnCartCount();
  }

  @override
  void onDispose() {
    if (subscription != null) {
      subscription.cancel();
    }
  }

  Stream<List<Item>> stream() {
    var res = getItemsStreamUseCase(UseCaseParam());
    if (res.isFailure) {
      throw res.message;
    }

    //subscription = res.data.listen((data) => null);

    return res.data;
  }

  Future updateItem(Item item, int amount) async {
    await updateItemCountStreamUseCase
        .call(UpdateItemCountParams(id: item.id, amount: amount));
    await _updateItemsOnCartCount();
  }

  Future _updateItemsOnCartCount() async {
    var items = await itemsStreamRepository.getItemsOnCart();

    if (items.isFailure) {
      throw Exception(items.message);
    }

    _nItemsOnCart = items.data.length;

    rebuild();
  }
}
