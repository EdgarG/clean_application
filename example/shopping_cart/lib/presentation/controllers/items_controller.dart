import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/usecases/clear_item_usecase.dart';
import 'package:shopping_cart/data/usecases/get_items_usecase.dart';
import 'package:shopping_cart/data/usecases/update_item_count_usecase.dart';
import 'package:shopping_cart/presentation/components/cart_icon.dart';
import 'package:shopping_cart/presentation/views/cart_view.dart';

class ItemsController extends Controller {
  GetItemsUseCase _itemsUseCase;
  UpdateItemCountUseCase _updateItemCountUseCase;
  ClearItemUseCase _clearItemUseCase;

  List<Item> _items;

  List<Item> get items => _items;
  bool get hasItems => _items != null && _items.isNotEmpty;

  List<Item> get itemsOnCart => _items.where((item) => item.count > 0).toList();

  int get countItemsOnCart => itemsOnCart.length;
  bool get hasItemsOnCart => countItemsOnCart > 0;

  @override
  Future onInitialize(dynamic args) async {
    _itemsUseCase = CleanApplication.usecase<GetItemsUseCase>();
    _updateItemCountUseCase =
        CleanApplication.usecase<UpdateItemCountUseCase>();
    _clearItemUseCase = CleanApplication.usecase<ClearItemUseCase>();

    _getItems();
  }

  Future _getItems() async {
    var res = await _itemsUseCase(UseCaseParam());

    if (res.isFailure) {
      throw res.message;
    }

    _items = res.data;
  }

  Future updateItem(Item item, int amount) async {
    var res = await _updateItemCountUseCase(
        UpdateItemCountParams(amount: amount, id: item.id));

    if (res.isFailure) {
      throw res.message;
    }

    var tags = [
      item.name,
      CartIcon.Tag,
    ];

    if (res.data.count == 0) {
      tags.add(CartView.Tag);
    }

    await _getItems();

    rebuild(tags);
  }

  Future clearItem(Item item) async {
    await _clearItemUseCase(ClearItemParams(id: item.id));
    await _getItems();
  }
}
