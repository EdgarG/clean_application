import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/services/theme_service.dart';

class HomeController extends Controller {
  @override
  Future onInitialize(args) {
    return null;
  }

  changeTheme() {
    ThemeService.changeTheme();
  }
}
