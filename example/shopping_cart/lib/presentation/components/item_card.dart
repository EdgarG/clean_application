import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/presentation/components/item_card_counter.dart';
import 'package:shopping_cart/presentation/controllers/home_controller.dart';
import 'package:shopping_cart/presentation/controllers/items_controller.dart';
import 'package:shopping_cart/util/random_color.dart';

class ItemCard extends StaticComponent<ItemsController> {
  final Item item;

  ItemCard({this.item});

  @override
  Widget build(BuildContext context, ItemsController controller) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 100,
          color: RandomColor.color(),
          child: Center(
            child: Text('${item.name}'),
          ),
        ),
        ItemCardCounter(
          index: controller.items.indexOf(item),
          tag: item.name,
        ),
      ],
    );
  }
}
