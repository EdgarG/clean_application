import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/presentation/components/cart_icon.dart';
import 'package:shopping_cart/presentation/controllers/home_controller.dart';
import 'package:shopping_cart/presentation/controllers/items_controller.dart';
import 'package:shopping_cart/presentation/views/cart_view.dart';
import 'package:shopping_cart/util/random_color.dart';

class ItemCardCounter extends DynamicComponent<ItemsController> {
  Item item;
  final int index;

  ItemCardCounter({this.index, String tag}) : super(tag: tag);

  @override
  Widget build(BuildContext context, ItemsController controller) {
    item = controller.items[index];

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        getItemButtom(item, false),
        Text(
          '${item.count}',
          style: TextStyle(color: RandomColor.color()),
        ),
        getItemButtom(item, true),
      ],
    );
  }

  Widget getItemButtom(Item item, bool isIncrementing) {
    if (isIncrementing) {
      return getButtom(
        true,
        Icons.arrow_right,
        () => controller.updateItem(item, 1),
      );
    }
    return getButtom(
      item.count > 0,
      Icons.arrow_left,
      () => controller.updateItem(item, -1),
    );
  }

  Widget getButtom(bool canUpdate, IconData iconData, VoidCallback callback) {
    return IconButton(
      icon: Icon(iconData),
      onPressed: canUpdate ? () => callback() : null,
    );
  }
}
