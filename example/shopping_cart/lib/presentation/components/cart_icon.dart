import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/presentation/controllers/home_controller.dart';
import 'package:shopping_cart/presentation/controllers/items_controller.dart';
import 'package:shopping_cart/presentation/views/cart_view.dart';

class CartIcon extends DynamicComponent<ItemsController> {
  static const String Tag = 'icon_cart';

  CartIcon() : super(tag: Tag);

  @override
  Widget build(BuildContext context, ItemsController controller) {
    if (!controller.hasItemsOnCart) {
      return Icon(Icons.add_shopping_cart);
    }

    return InkWell(
      onTap: () => router.pushType(CartView),
      child: Container(
        alignment: Alignment.center,
        child: Stack(
          children: <Widget>[
            Icon(Icons.shopping_cart),
            getCountIndicator(),
          ],
        ),
      ),
    );
  }

  Widget getCountIndicator() {
    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
        color: Colors.red,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(
        child: Text(
          '${controller.countItemsOnCart}',
          style: TextStyle(fontSize: 10),
        ),
      ),
    );
  }
}
