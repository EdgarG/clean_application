import 'dart:math';

import 'package:flutter/material.dart';

class RandomColor {
  static Random random = Random();
  static Color color() {
    return Color.fromRGBO(
        random.nextInt(250), random.nextInt(250), random.nextInt(250), 1);
  }
}
