import 'package:clean_application/clean_application.dart';
import 'package:flutter/material.dart';
import 'package:shopping_cart/config/app_config.dart';
import 'package:shopping_cart/presentation/views/home_view.dart';
import 'package:shopping_cart/services/theme_service.dart';

void main() =>
    runApp(CleanApplication(configuration: AppConfig(), child: MyApp()));

class MyApp extends View<MyAppController> {
  @override
  Widget build(BuildContext context, MyAppController controller) {
    ThemeService themeService = CleanApplication.service<ThemeService>();

    return MaterialApp(
      title: 'Shopping Cart',
      theme: themeService.currentTheme,
      debugShowCheckedModeBanner: false,
      initialRoute: '$HomeTagView',
      onGenerateRoute: CleanApplication.generateRoutes,
      navigatorKey: CleanApplication.navigatorKey,
    );
  }
}

class MyAppController extends Controller {
  @override
  Future onInitialize(args) {
    return null;
  }
}
