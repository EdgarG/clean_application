import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/result.dart';
import 'package:clean_application/src/domain/entity.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_stream_repository.dart';
import 'package:shopping_cart/domain/datasources/item_stream_datasource.dart';

class ItemStreamRepositoryImpl extends ItemsStreamRepository {
  final ItemStreamDataSource _dataSource;

  ItemStreamRepositoryImpl(this._dataSource);

  @override
  void init() {
    _dataSource.init();
  }

  @override
  Result<Stream<List<Item>>> streamAll() {
    var res = _dataSource.streamAll();
    if (res.isFailure) {
      return Result<Stream<List<Item>>>.failure(res.message);
    }

    return Result<Stream<List<Item>>>.success(
      res.data.map<List<Item>>(
        (models) => models.map((model) => model.toEntity()).toList(),
      ),
    );
  }

  @override
  Result<Stream<Item>> streamById(int id) {
    // TODO: implement streamById
    return null;
  }

  @override
  void dispose() {
    _dataSource.dispose();
  }

  @override
  Future<Result<Item>> getItemById(int id) async {
    var res = await _dataSource.getModel(id);

    if (res.isFailure) {
      return Result<Item>.failure(res.message);
    }

    return Result<Item>.success(res.data.toEntity());
  }

  @override
  Future<Result<bool>> clearModel(int id) async {
    return await _dataSource.clearModel(id);
  }

  @override
  Future<Result<Item>> updateModel(int id, int amount) async {
    var res = await _dataSource.updateModel(id, amount);
    if (res.isFailure) {
      return Result<Item>.failure(res.message);
    }
    return Result<Item>.success(res.data.toEntity());
  }

  @override
  Future<Result<List<Item>>> getItemsOnCart() async {
    var res = await _dataSource.getItemsOnCart();
    if (res.isFailure) {
      return Result<List<Item>>.failure(res.message);
    }
    return Result<List<Item>>.success(
        res.data.map((model) => model.toEntity()).toList());
  }
}
