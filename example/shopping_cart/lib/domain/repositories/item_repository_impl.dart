import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/result.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_repository.dart';
import 'package:shopping_cart/domain/datasources/item_datasource.dart';

class ItemsRepositoryImpl extends ItemsRepository {
  final ItemDataSource _dataSource;

  ItemsRepositoryImpl(this._dataSource);

  @override
  Future<Result<bool>> clearItem(int id) async {
    return await _dataSource.clearModel(id);
  }

  @override
  Future<Result<Item>> getItemById(int id) async {
    var res = await _dataSource.getModel(id);

    if (res.isFailure) {
      return Result<Item>.failure(res.message);
    }

    return Result<Item>.success(res.data.toEntity());
  }

  @override
  Future<Result<List<Item>>> getItems() async {
    var res = await _dataSource.getModels();

    if (res.isFailure) {
      return Result<List<Item>>.failure(res.message);
    }

    return Result.success(res.data.map((model) => model.toEntity()).toList());
  }

  @override
  Future<Result<Item>> updateItem(int id, int amount) async {
    var res = await _dataSource.updateModel(id, amount);

    if (res.isFailure) {
      return Result<Item>.failure(res.message);
    }

    return Result.success(res.data.toEntity());
  }
}
