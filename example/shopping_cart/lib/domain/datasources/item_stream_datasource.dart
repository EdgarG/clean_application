import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/domain/models/item_model.dart';

abstract class ItemStreamDataSource extends StreamDataSource<ItemModel, int> {
  void init();

  Future<Result<ItemModel>> getModel(int id);

  Future<Result<ItemModel>> updateModel(int id, int amount);

  Future<Result<bool>> clearModel(int id);

  Future<Result<List<ItemModel>>> getItemsOnCart();
}
