import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/domain/models/item_model.dart';

abstract class ItemDataSource extends DataSource<ItemModel, int> {
  Future<Result<List<ItemModel>>> getModels();

  Future<Result<ItemModel>> getModel(int id);

  Future<Result<ItemModel>> updateModel(int id, int amount);

  Future<Result<bool>> clearModel(int id);
}
