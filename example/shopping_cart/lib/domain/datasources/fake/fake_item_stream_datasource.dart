import 'dart:async';

import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/result.dart';
import 'package:shopping_cart/domain/datasources/item_stream_datasource.dart';
import 'package:shopping_cart/domain/models/item_model.dart';

class FakeItemStreamDatasource extends ItemStreamDataSource {
  StreamController<List<ItemModel>> controller;

  List<ItemModel> _models = [
    ItemModel(id: 1, name: "Item 1"),
    ItemModel(id: 2, name: "Item 2"),
    ItemModel(id: 3, name: "Item 3"),
    ItemModel(id: 4, name: "Item 4"),
    ItemModel(id: 5, name: "Item 5"),
    ItemModel(id: 6, name: "Item 6"),
  ];

  FakeItemStreamDatasource() {
    controller = StreamController.broadcast(
      onListen: () => init(),
      onCancel: () => null,
    );
  }

  @override
  void init() {
    controller.add(_models);
  }

  @override
  Result<Stream<List<ItemModel>>> streamAll() {
    return Result<Stream<List<ItemModel>>>.success(controller.stream);
  }

  @override
  void dispose() {
    print('Disposing the stream controller');
    controller.close();
  }

  @override
  Future<Result<bool>> clearModel(int id) async {
    var model = _models.firstWhere((it) => it.id == id);

    if (model == null) {
      return Result<bool>.success(false);
    }

    int index = _models.indexOf(model);
    _models.replaceRange(
      index,
      index + 1,
      [ItemModel(id: model.id, name: model.name)],
    );

    controller.add(_models);

    return Result<bool>.success(true);
  }

  @override
  Future<Result<ItemModel>> updateModel(int id, int amount) async {
    var model = _models.firstWhere((it) => it.id == id);

    if (model == null) {
      return Result<ItemModel>.failure('The item with id $id does not exist');
    }

    var newModel =
        ItemModel(id: model.id, name: model.name, count: model.count + amount);

    int index = _models.indexOf(model);

    _models.replaceRange(
      index,
      index + 1,
      [newModel],
    );

    controller.add(_models);

    return Result<ItemModel>.success(newModel);
  }

  @override
  Future<Result<ItemModel>> getModel(int id) async {
    var model = _models.firstWhere((it) => it.id == id);

    if (model == null) {
      return Result<ItemModel>.failure(
          'The model with id: $id does not exist!');
    }

    return Result<ItemModel>.success(model);
  }

  @override
  Result<Stream<ItemModel>> streamById(int id) {
    // TODO: implement streamById
    return null;
  }

  @override
  Future<Result<List<ItemModel>>> getItemsOnCart() async {
    return Result<List<ItemModel>>.success(
        _models.where((model) => model.count > 0).toList());
  }
}
