import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';

class ItemModel extends ModelMap<Item> {
  final int id;
  final String name;
  final int count;

  ItemModel({
    this.id,
    this.name,
    this.count = 0,
  });

  @override
  Model<Item, Map> fromDataSource(Map object) {
    return ItemModel(
      id: object["id"],
      name: object["name"],
      count: object["count"],
    );
  }

  @override
  Model<Item, Map> fromEntity(Item entity) {
    return ItemModel(
      id: entity.id,
      name: entity.name,
      count: entity.count,
    );
  }

  @override
  Map toDataSource() {
    return {"id": id, "name": name, "count": count};
  }

  @override
  Item toEntity() {
    return Item(
      id: id,
      name: name,
      count: count,
    );
  }
}
