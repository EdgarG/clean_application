import 'dart:developer';

import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/service.dart';
import 'package:shopping_cart/data/repository_contracts/items_repository.dart';
import 'package:shopping_cart/data/usecases/clear_item_usecase.dart';
import 'package:shopping_cart/data/usecases/get_item_usecase.dart';
import 'package:shopping_cart/data/usecases/get_items_usecase.dart';
import 'package:shopping_cart/data/usecases/update_item_count_stream_usecase.dart';
import 'package:shopping_cart/data/usecases/update_item_count_usecase.dart';
import 'package:shopping_cart/data/usecases/get_items_stream_usecase.dart';
import 'package:shopping_cart/domain/datasources/fake/fake_item_datasource.dart';
import 'package:shopping_cart/domain/datasources/fake/fake_item_stream_datasource.dart';
import 'package:shopping_cart/domain/datasources/item_datasource.dart';
import 'package:shopping_cart/domain/datasources/item_stream_datasource.dart';
import 'package:shopping_cart/domain/repositories/item_repository_impl.dart';
import 'package:shopping_cart/domain/repositories/items_stream_repository_impl.dart';
import 'package:shopping_cart/main.dart';
import 'package:shopping_cart/presentation/controllers/home_controller.dart';
import 'package:shopping_cart/presentation/controllers/items_controller.dart';
import 'package:shopping_cart/presentation/controllers/items_stream_controller.dart';
import 'package:shopping_cart/presentation/views/cart_view.dart';
import 'package:shopping_cart/presentation/views/home_view.dart';
import 'package:shopping_cart/data/repository_contracts/items_stream_repository.dart';
import 'package:shopping_cart/presentation/views/items_stream_view.dart';
import 'package:shopping_cart/presentation/views/items_view.dart';
import 'package:shopping_cart/services/theme_service.dart';

class AppConfig extends Configuration {
  @override
  List<Factory<Controller>> controllers() {
    return [
      Factory<MyAppController>(factory: () => MyAppController()),
      Factory<HomeController>(factory: () => HomeController()),
      Factory<ItemsController>(factory: () => ItemsController()),
      Factory<ItemsStreamController>(factory: () => ItemsStreamController()),
    ];
  }

  @override
  List<Implementation<DataSource>> datasources() {
    return [
      Implementation<ItemDataSource>(
        instances: {
          Environments.All: () => FakeItemDataSource(),
        },
      ),
      Implementation<ItemStreamDataSource>(
        instances: {
          Environments.All: () {
            var instance = FakeItemStreamDatasource();
            //instance.init();
            return instance;
          },
        },
      ),
    ];
  }

  @override
  List<Implementation<Repository>> repositories() {
    return [
      Implementation<ItemsRepository>(
        instances: {
          Environments.All: () => ItemsRepositoryImpl(
                CleanApplication.datasource<ItemDataSource>(),
              ),
        },
      ),
      Implementation<ItemsStreamRepository>(
        instances: {
          Environments.All: () => ItemStreamRepositoryImpl(
              CleanApplication.datasource<ItemStreamDataSource>()),
        },
      ),
    ];
  }

  @override
  List<RouterBuilder> routes() {
    return [
      RouterBuilder<HomeTagView>(builder: (args) => HomeTagView()),
      RouterBuilder<ItemsView>(builder: (args) => ItemsView()),
      RouterBuilder<CartView>(builder: (args) => CartView()),
      RouterBuilder<ItemsStreamView>(builder: (args) => ItemsStreamView()),
    ];
  }

  @override
  List<UseCase> usecases() {
    return [
      GetItemUseCase(),
      GetItemsUseCase(),
      ClearItemUseCase(),
      UpdateItemCountUseCase(),
      GetItemsStreamUseCase(),
      UpdateItemCountStreamUseCase(),
    ];
  }

  @override
  List<Factory<Service>> services() {
    return [
      Factory<ThemeService>(factory: () => ThemeService()),
    ];
  }
}
