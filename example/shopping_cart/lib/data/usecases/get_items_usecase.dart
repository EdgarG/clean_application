import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_repository.dart';

class GetItemsUseCase extends FutureUseCase<List<Item>, UseCaseParam> {
  @override
  Future<Result<List<Item>>> call(UseCaseParam params) async {
    ItemsRepository repository = CleanApplication.repository<ItemsRepository>();
    return await repository.getItems();
  }
}
