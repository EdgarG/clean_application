import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_stream_repository.dart';

class GetItemsStreamUseCase extends StreamUseCase<List<Item>, UseCaseParam> {
  @override
  Result<Stream<List<Item>>> call(UseCaseParam params) {
    ItemsStreamRepository repository =
        CleanApplication.repository<ItemsStreamRepository>();
    var res = repository.streamAll();

    if (res.isFailure) {
      return Result<Stream<List<Item>>>.failure(res.message);
    }

    return Result<Stream<List<Item>>>.success(res.data);

    //return Result<Stream<List<Item>>>.success(res.data.map((e) => e as List<Item>));
  }
}
