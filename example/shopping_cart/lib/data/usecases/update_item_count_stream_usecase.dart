import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_repository.dart';
import 'package:shopping_cart/data/repository_contracts/items_stream_repository.dart';
import 'package:shopping_cart/data/usecases/get_item_usecase.dart';

class UpdateItemCountParams extends UseCaseParam {
  final int id;
  final int amount;

  UpdateItemCountParams({
    this.id,
    this.amount,
  })  : assert(id != null),
        assert(amount != null);
}

class UpdateItemCountStreamUseCase
    extends FutureUseCase<Item, UpdateItemCountParams> {
  @override
  Future<Result<Item>> call(UpdateItemCountParams params) async {
    ItemsStreamRepository repository =
        CleanApplication.repository<ItemsStreamRepository>();

    var res = await repository.getItemById(params.id);

    if (res.isFailure) {
      return Result<Item>.failure(res.message);
    }

    var item = res.data;

    if (item.count + params.amount < 0) {
      return Result<Item>.failure(
        'The item count(${item.count}) can not update with amount(${params.amount}) because it will be negative',
      );
    }

    return await repository.updateModel(params.id, params.amount);
  }
}
