import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';
import 'package:shopping_cart/data/repository_contracts/items_repository.dart';

class GetItemParams extends UseCaseParam {
  final int id;

  GetItemParams({this.id}) : assert(id != null);
}

class GetItemUseCase extends FutureUseCase<Item, GetItemParams> {
  @override
  Future<Result<Item>> call(GetItemParams params) async {
    ItemsRepository repository = CleanApplication.repository<ItemsRepository>();
    return await repository.getItemById(params.id);
  }
}
