import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/repository_contracts/items_repository.dart';

class ClearItemParams extends UseCaseParam {
  final int id;

  ClearItemParams({this.id}) : assert(id != null);
}

class ClearItemUseCase extends FutureUseCase<bool, ClearItemParams> {
  @override
  Future<Result<bool>> call(ClearItemParams params) async {
    ItemsRepository repository = CleanApplication.repository<ItemsRepository>();
    return await repository.clearItem(params.id);
  }
}
