import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';

abstract class ItemsRepository extends Repository<Item, int> {
  Future<Result<List<Item>>> getItems();

  Future<Result<Item>> getItemById(int id);

  Future<Result<Item>> updateItem(int id, int amount);

  Future<Result<bool>> clearItem(int id);
}
