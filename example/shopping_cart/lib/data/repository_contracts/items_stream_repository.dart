import 'package:clean_application/clean_application.dart';
import 'package:shopping_cart/data/entities/item.dart';

abstract class ItemsStreamRepository extends StreamRepository<Item, int> {
  void init();

  Future<Result<Item>> getItemById(int id);

  Future<Result<Item>> updateModel(int id, int amount);

  Future<Result<bool>> clearModel(int id);

  Future<Result<List<Item>>> getItemsOnCart();
}
