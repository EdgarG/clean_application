import 'package:clean_application/clean_application.dart';

class Item extends Entity<int> {
  String name;
  int count;

  Item({
    int id,
    this.name,
    this.count = 0,
  }) : super(id: id);

  @override
  String toString() {
    return 'Item{id: $id, name: $name, count: $count}';
  }
}
