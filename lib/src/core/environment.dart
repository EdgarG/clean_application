/// Environments to be registered for interfaces implementations
class Environments {
  static const String All = "All";
  static const String Development = "dev";
  static const String Production = "prod";
  static const String Test = "test";
}
