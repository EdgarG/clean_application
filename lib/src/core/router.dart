import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// A wrapper class around the [NavigatorState]
/// The main purpose is to not use allways a [BuildContext] to navigate between
/// [View]s.
/// It's easy to navigate between views using their class type.
/// example: router.pushType(HomeView, arguments: 'Title')
///
class Router {
  final GlobalKey<NavigatorState> _key = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get key => _key;

  Future<dynamic> push(Widget widget) {
    return _key.currentState
        .push(MaterialPageRoute(builder: (context) => widget));
  }

  Future<dynamic> pushNamed(String routeName, {dynamic arguments}) {
    return _key.currentState.pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> pushType(Type type, {dynamic arguments}) {
    return _key.currentState.pushNamed('$type', arguments: arguments);
  }

  void pop({dynamic result}) {
    _key.currentState.pop(result);
  }

  Future<dynamic> replace(Widget widget, {bool pop = false, dynamic result}) {
    if (pop) {
      this.pop(result: result);
    }
    return _key.currentState.pushReplacement(MaterialPageRoute(
      builder: (_) => widget,
    ));
  }

  Future<dynamic> replaceNamed(String routeName,
      {dynamic arguments, bool pop = false, dynamic result}) {
    if (pop) {
      this.pop(result: result);
    }
    return _key.currentState
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  Future<dynamic> replaceType(Type type,
      {dynamic arguments, bool pop = false, dynamic result}) {
    return replaceNamed('$type',
        arguments: arguments, pop: pop, result: result);
  }

  Future<dynamic> removeUntil(Widget widget) {
    return _key.currentState.pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => widget), (route) => false);
  }

  Future<dynamic> removeUntilNamed(String routeName, {dynamic arguments}) {
    return _key.currentState.pushNamedAndRemoveUntil(
        routeName, (route) => false,
        arguments: arguments);
  }

  Future<dynamic> removeUntilType(Type type, {dynamic arguments}) {
    return removeUntilNamed('$type', arguments: arguments);
  }
}
