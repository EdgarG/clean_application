import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/factory.dart';
import 'package:clean_application/src/core/route_builder.dart';
import 'package:clean_application/src/core/service.dart';
import 'package:clean_application/src/data/datasource.dart';
import 'package:clean_application/src/domain/repository.dart';
import 'package:clean_application/src/domain/usecase.dart';
import 'package:clean_application/src/core/implementation.dart';

/// This class provides the dependencies to be injected in the application
abstract class Configuration {
  /// If true all the Routes will be wrapped with a safe area
  bool wrapViewWithSafeArea() => true;

  /// Data Sources
  List<Implementation<DataSource>> datasources();

  /// Repositories
  List<Implementation<Repository>> repositories();

  /// Use cases
  List<UseCase> usecases();

  /// Controllers for each view
  List<Factory<Controller>> controllers();

  /// Application routes/views
  List<RouterBuilder> routes();

  /// Custom services
  List<Factory<Service>> services();
}
