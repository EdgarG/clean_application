import 'package:flutter/cupertino.dart';

/// This class provides a widget view according to the routes registered in the
/// [Configuration]
///
class RouterBuilder<T> {
  String _name;
  final Widget Function(dynamic arguments) builder;

  RouterBuilder({
    this.builder,
  }) : assert(builder != null) {
    _name = '$T';
  }

  String get name => _name;
}
