import 'package:collection/collection.dart';

/// This class is copied from Equatable but without the immutable.
/// The immutable property was removed because very often we need to change the
/// object/entity in our application.
abstract class Equals {
  /// The [List] of `props` (properties) which will be used to determine whether
  /// two [Equals] are equal.
  List<Object> get properties;

  /// A class that helps implement equality
  /// without needing to explicitly override == and [hashCode].
  /// Equals override their own `==` operator and [hashCode] based on their `properties`.
  Equals();

  int mapPropsToHashCode(Iterable props) =>
      _finish(props.fold(0, (hash, object) => _combine(hash, object)));

  DeepCollectionEquality _equality = DeepCollectionEquality();

  /// Jenkins Hash Functions
  /// https://en.wikipedia.org/wiki/Jenkins_hash_function
  int _combine(int hash, dynamic object) {
    if (object is Map) {
      object.forEach((key, value) {
        hash = hash ^ _combine(hash, [key, value]);
      });
      return hash;
    }
    if (object is Iterable) return mapPropsToHashCode(object);
    hash = 0x1fffffff & (hash + object.hashCode);
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  int _finish(int hash) {
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }

  bool equals(List list1, List list2) {
    if (identical(list1, list2)) return true;
    if (list1 == null || list2 == null) return false;
    int length = list1.length;
    if (length != list2.length) return false;

    for (int i = 0; i < length; i++) {
      final unit1 = list1[i];
      final unit2 = list2[i];

      if (unit1 is Iterable || unit1 is Map) {
        if (!_equality.equals(unit1, unit2)) return false;
      } else if (unit1?.runtimeType != unit2?.runtimeType) {
        return false;
      } else if (unit1 != unit2) {
        return false;
      }
    }
    return true;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Equals &&
          runtimeType == other.runtimeType &&
          equals(properties, other.properties);

  @override
  int get hashCode => runtimeType.hashCode ^ mapPropsToHashCode(properties);

  @override
  String toString() => '$runtimeType';
}
