/// All call should return a result object.
class Result<T> {
  bool hasError;
  String message;
  T data;

  Result({this.hasError, this.message, this.data});

  bool get isSuccess => hasError == null || hasError == false;
  bool get isFailure => hasError == true;

  factory Result.success([T data]) {
    return Result(hasError: false, data: data);
  }

  factory Result.failure(String message) {
    return Result(hasError: true, message: message);
  }

  @override
  String toString() {
    return 'Result{hasError: $hasError, message: $message, data: $data}';
  }
}
