import 'package:clean_application/clean_application.dart';
import 'package:clean_application/src/core/factory.dart';
import 'package:clean_application/src/core/router.dart';
import 'package:clean_application/src/core/service.dart';
import 'package:clean_application/src/data/datasource.dart';
import 'package:clean_application/src/domain/repository.dart';
import 'package:clean_application/src/domain/usecase.dart';
import 'package:clean_application/src/core/configuration.dart';
import 'package:clean_application/src/presentation/controller.dart';
import 'package:clean_application/src/core/environment.dart';
import 'package:clean_application/src/core/implementation.dart';
import 'package:flutter/material.dart';

/// This class is the root of the application
/// It register all the datasources, repositories, use cases and controllers.
/// The above order is the order in which the registration is done because of the
/// Clean architecture call flow.
/// The public static methods is used to access the registered instance on each
/// layer.
///
class CleanApplication extends InheritedModel {
  static Map<String, DataSource> _datasources = Map();
  static Map<String, Repository> _repositories = Map();
  static Map<String, UseCase> _usecases = Map();
  static Map<String, Factory> _controllers = Map();
  static Map<String, Factory> _services = Map();

  static Map<String, RouterBuilder> _routes = Map();

  final String environment;
  static bool _wrapViewWithSafeArea;

  final Configuration configuration;

  static final Router _router = Router();

  CleanApplication({
    this.environment = Environments.Development,
    @required this.configuration,
    @required Widget child,
  })  : assert(environment != null),
        assert(configuration != null),
        super(child: child) {
    _wrapViewWithSafeArea = configuration.wrapViewWithSafeArea() ?? true;

    /// This call order is important because of the layers dependency (call flow)
    _initializeImplemantations(configuration.datasources(), _datasources);
    _initializeImplemantations(configuration.repositories(), _repositories);
    _initializeList(configuration.usecases(), _usecases);

    /// Initialize the factories
    _initializeFactory(configuration.controllers(), _controllers);
    _initializeFactory(configuration.services(), _services);

    /// Initialize routes
    _initializeRoutes();
  }

  static Router get router => _router;
  static GlobalKey<NavigatorState> get navigatorKey => _router.key;
  static NavigatorState get navigator => _router.key.currentState;

  /// Register each interface implementations
  /// Intended for [DataSource] and [Repository] implementations.
  void _initializeImplemantations(
      List<Implementation> implementations, Map map) {
    for (Implementation implementation in implementations) {
      _initializeImplementation(implementation, map);
    }
  }

  /// Register an instance for an interface according to the current environment
  /// or for the Environment.All if registered in the instances map
  void _initializeImplementation(Implementation implementation, Map map) {
    InstanceBuilder builder;

    if (implementation.instances.containsKey(environment)) {
      builder = implementation.instances[environment];
    } else if (implementation.instances.containsKey(Environments.All)) {
      builder = implementation.instances[Environments.All];
    }

    if (builder != null) {
      _injectInMap(map, builder(), implementation.name);
    }
  }

  /// Initialize a list that don't have instance builder, just the instance
  /// itself. Intended for non interface like [Controller] and [UseCase]
  void _initializeList(List list, Map map) {
    if (list != null) {
      for (var val in list) {
        _injectInMap(map, val, '${val.runtimeType}');
      }
    }
  }

  /// Injects an instance on a map
  void _injectInMap<T>(Map map, T instance, [String name]) {
    if (name != null && name.isNotEmpty) {
      map[name] = instance;
    } else {
      map['$T'] = instance;
    }
  }

  /// Get an instance of the type [T] if exists on the map
  static _getFromMap<T>(Map map, [String name]) {
    T value;
    if (name != null && name.isNotEmpty) {
      value = map[name] as T;
    } else {
      value = map['$T'] as T;
    }

    return value;
  }

  /// Register all Factory in the given map
  void _initializeFactory(List<Factory> list, Map map) {
    if (list == null) {
      return;
    }

    for (var factory in list) {
      map[factory.name] = factory;
    }
  }

  /// Register all routes provided in the [Configuration]
  void _initializeRoutes() {
    if (configuration.routes() == null) {
      return;
    }

    for (var route in configuration.routes()) {
      _routes[route.name] = route;
    }
  }

  /// Return a [DataSource] of type [T] if registered
  static datasource<T>([String name]) {
    return _getFromMap<T>(_datasources, name);
  }

  /// Return a [Repository] of type [T] if registered
  static repository<T>([String name]) {
    return _getFromMap<T>(_repositories, name);
  }

  /// Return an [Usecase] of type [T] if registered
  static usecase<T>([String name]) {
    return _getFromMap<T>(_usecases, name);
  }

  /// Return a [Controller] of type [T] if registered
  /// Every time it is called it returns a new [T] instance if it's not
  /// a singleton
  static controller<T extends Controller>([String name]) {
    Factory<T> factory;

    if (name != null && name.isNotEmpty) {
      factory = _controllers[name];
    } else {
      factory = _controllers['$T'];
    }

    if (factory == null) {
      return null;
    }

    if (factory.isSingleton) {
      if (!factory.hasSingleton) {
        factory.initializeSingleton();
      }

      return factory.singleton;
    }

    return factory.factory();
  }

  /// Return a [Service] of type [T] if registered
  /// Every time it is called it returns a new [T] instance if it's not
  /// a singleton
  static service<T extends Service>([String name]) {
    Factory<T> factory;

    if (name != null && name.isNotEmpty) {
      factory = _services[name];
    } else {
      factory = _services['$T'];
    }

    if (factory == null) {
      return null;
    }

    if (factory.isSingleton) {
      if (!factory.hasSingleton) {
        factory.initializeSingleton();
      }

      return factory.singleton;
    }

    return factory.factory();
  }

  static CleanApplication of(BuildContext context, {dynamic aspect}) {
    return InheritedModel.inheritFrom<CleanApplication>(context,
        aspect: aspect);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  @override
  bool updateShouldNotifyDependent(InheritedModel oldWidget, Set dependencies) {
    return true;
  }

  /// On generate a route it is called the corresponding builder registered with
  /// the [Configuration]
  /// It provides an not defined route in cause a [RouteSettings] name does not
  /// exist in the [RouterBuilder] map.
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    RouterBuilder builder = _routes[settings.name];

    if (builder == null) {
      return _buildView(Scaffold(
        body: Center(
          child: Text("No route defined for ${settings.name}"),
        ),
      ));
    }

    return _buildView(builder.builder(settings.arguments));
  }

  /// Return a [RouterBuilder] for type [T] if it is registered
  static RouterBuilder route<T>() {
    return _routes['$T'] ?? null;
  }

  /// Return a router name for type [T] if it is registered
  static String routeName<T>() {
    return _routes['$T']?.name ?? null;
  }

  /// Return a [MaterialPageRoute] for a view
  static MaterialPageRoute _buildView(Widget view) {
    return MaterialPageRoute(builder: (context) {
      if (_wrapViewWithSafeArea) {
        return SafeArea(child: view);
      }

      return view;
    });
  }
}
