import 'package:clean_application/src/data/model.dart';
import 'package:clean_application/src/core/result.dart';

abstract class DataSource<ModelType extends Model, IdType> {}

abstract class CrudDataSource<ModelType extends Model, IdType>
    extends DataSource<ModelType, IdType> {
  /// Returns the numbers of entities available
  Future<Result<int>> count();

  /// Deletes a given entity
  Future<Result<bool>> delete(ModelType model);

  /// Deletes all entities managed by the repository.
  Future<Result<bool>> deleteAll();

  /// Deletes the given entities.
  Future<Result<bool>> deleteEntities(Iterable<ModelType> models);

  /// Deletes the entity with the given id.
  Future<Result<bool>> deleteById(IdType id);

  /// Returns whether an entity with the given id exists.
  Future<Result<bool>> existsById(IdType id);

  /// Returns all instances of the type.
  Future<Result<Iterable<ModelType>>> findAll();

  /// Returns all instances of the type T with the given IDs.
  Future<Result<Iterable<ModelType>>> findAllById(Iterable<IdType> ids);

  /// Retrieves an entity by its id.
  Future<Result<ModelType>> findById(IdType id);

  /// Saves a given entity.
  Future<Result<ModelType>> save(ModelType model);

  /// Saves all given entities.
  Future<Result<Iterable<ModelType>>> saveAll(Iterable<ModelType> models);
}

abstract class StreamDataSource<ModelType extends Model, IdType>
    extends DataSource<ModelType, IdType> {
  Result<Stream<ModelType>> streamById(IdType id);

  Result<Stream<List<ModelType>>> streamAll();

  void dispose();
}
