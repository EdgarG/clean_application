import 'package:clean_application/clean_application.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ControllerNotifierData extends Equals {
  final List<String> tags;
  final bool value;

  ControllerNotifierData({
    this.tags,
    this.value,
  });

  List<Object> get properties => [value];
}

enum ControllerState {
  BUSY, IDLE
}

/// Controller class that hold a View business logic
abstract class Controller {
  ValueNotifier<ControllerNotifierData> _valueNotifier;
  bool _initialized = false;
  ControllerState _state;

  dynamic _args;

  Controller() {
    _state = ControllerState.IDLE;
    _valueNotifier = ValueNotifier(ControllerNotifierData(value: false));
    _initialized = false;
  }

  ValueNotifier<ControllerNotifierData> get notifier => _valueNotifier;

  /// Called when the View init state is mounted
  /// This can be overrided to perform some initialization tasks
  Future<void> initialize(dynamic args) async {
    _args = args;
    setBusy();

    await onInitialize(args);

    _initialized = true;
    setIdle();
  }

  Future<void> reinitialize() async {
    setBusy();
    await onInitialize(_args);
    setIdle();

    rebuild();
  }

  void setBusy() => _setState(ControllerState.BUSY);
  void setIdle() => _setState(ControllerState.IDLE);

  void _setState(ControllerState newState) {
    setState(() => _state = newState);
  }

  /// Called when the [_ViewState] is initialized
  Future onInitialize(dynamic args);

  /// To be implemented when is necessary to clean variables (ex: Streams)
  void onDispose() {}

  bool get isInitialized => _initialized == true;
  bool get isNotInitialized => _initialized == false;
  bool get isBusy  => _state == ControllerState.BUSY;
  bool get isIdle  => _state == ControllerState.IDLE;

  Router get router => CleanApplication.router;

  NavigatorState get navigator => CleanApplication.navigator;

  BuildContext get viewContext => navigator.context;


  /// Use to explicitly rebuild a view from the controller.
  void rebuild([List<String> tags]) {
    setState(null, tags);
  }

  /// Call a controller method
  /// And then notify listeners the tags is the [View] tag
  void setState(VoidCallback callback, [List<String> tags]) {

    if (callback != null) {
      callback();
    }
    _valueNotifier.value =
        ControllerNotifierData(value: !_valueNotifier.value.value, tags: tags);
  }

  static service<T extends Service>([String name]){
    return CleanApplication.service(name);
  }

  static usecase<T>([String name]) {
    return CleanApplication.usecase(name);
  }

  static controller<T extends Controller>([String name]) {
    return CleanApplication.controller(name);
  }

}
