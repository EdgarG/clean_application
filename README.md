# clean_application

Clean Architecture package for Flutter Apps.

## Getting Started

The project structure:
|-- lib
|   |-- clean_application.dart
|   '-- src
|       |-- core
|       |   |-- clean_application.dart
|       |   |-- configuration.dart
|       |   |-- environment.dart
|       |   |-- equals.dart
|       |   |-- factory.dart
|       |   |-- implementation.dart
|       |   |-- result.dart
|       |   |-- route_builder.dart
|       |   |-- router.dart
|       |   |-- service.dart
|       |   '-- stream_converter.dart
|       |-- data
|       |   |-- datasource.dart
|       |   '-- model.dart
|       |-- domain
|       |   |-- entity.dart
|       |   |-- repository.dart
|       |   '-- usecase.dart
|       '-- presentation
|           |-- component.dart
|           |-- controller.dart
|           '-- view.dart

