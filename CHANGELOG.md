## [1.0.0+1] - Bug fix.

## [2.0.0] - New features.
* Controller Factory
* Service Factory
* DataSource Implementation
* Repository Implementation
* RouteBuilder (navigator system - Can navigate by view type)
* View rebuild the state are controlled with view Tags
* AnimatedView
* Controller initialize with arguments from the view

## [2.0.0+1] - BuildContext added to views.

## [2.1.0] - Changes according to Flutter v1.16.3
* Router pop changed to return void because flutter 1.16.3 does not return bool anymore
* RouteBuilder changed to RouterBuilder because of name conflict with material RouteBuilder

## [2.1.0+1] pub.dev specification
* collection: ^1.14.12 changed to collection: ^1.14.11

## [2.1.0+2] reinitialize controller
## [2.1.0+3] Minor improvements
* Call setState on view only if it is mounted to prevent bug on runtime
* New enum ControllerState with BUSY or IDLE state for controller

## [2.1.0+4] Minor improvements
* Controller get methods isBusy and isIdle

## [2.1.0+5] Wrapper functions of CleanApplication inside Controller
* service<T>, usecase<T>, controller<T>

## [2.1.0+6] Wrapper functions of CleanApplication 
* In UseCase
* In Repository
